<?php
if(count($model->pictures)>0): ?>
    <div class="box box-table">
        <table class="table">
            <thead>
            <tr>
                <th width="10%"></th>
                <th width="15%">Image</th>
                <th width="5%">Show in Slider?</th>
                <th width="5%" class="text-center">Delete</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($model->pictures as $obj){ ?>
                <tr data-snap="<?=$obj->id?>" class="snapshot">
                    <td></td>
                    <td><img src="<?=UPLOAD_URL.'Snapshots'.DS.$obj->image?>"></td>
                    <td><input id='snap-<?=$obj->id?>' name="in_slider" value="1" disabled type="checkbox" <?=$obj->in_slider ? 'checked' : ''?>></td>
                    <td class="text-center">
                        <a class="btn-actions" href="<?= ADMIN_URL ?>pictures/delete/<?= $obj->id ?>?token_id=<?php echo get_token();?>" onClick="return confirm('Are You Sure?');">
                            <i class="icon-cancel-circled"></i>
                        </a>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <div class="box-footer clearfix">
            <div class='paginationContent'></div>
        </div>
    </div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
    var site_url = '<?= ADMIN_URL.'snapshots';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>
<script>
    $(document).ready(function(){
        $('.snapshot').click(function(e){
           var id=$(this).data('snap');
           var in_slider = $('#snap-'+id).prop('checked') ? 0 : 1;
           $.post('/admin/pictures/update', {id:id,in_slider:in_slider},function (data){
               $('#snap-'+id).prop('checked',in_slider);
           });
       });
    });
</script>