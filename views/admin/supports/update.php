<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
	<div class="tab-content">
		<div role="tabpanel" class="tab-pane active">
			<input type="hidden" name="id" value="<?php echo $model->support->id; ?>" />
			<input name="token" type="hidden" value="<?php echo get_token();?>" />
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<h4>Support Details</h4>
						<div class="form-group">
                            <label>Support Category</label>
                            <?php echo $model->form->editorFor("support_list"); ?>
                        </div>
						<div class="form-group">
                            <label>Phone</label>
                            <?php echo $model->form->editorFor("phone"); ?>
                        </div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-save">Save</button>
	</div>
</form>