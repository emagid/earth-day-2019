<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
  <input type="hidden" name="id" value="<?php echo $model->ring->id;?>" />
  <input type="hidden" name="price" value="<?php echo $model->ring->price;?>" />
  <input type="hidden" name="cost_per_dwt" value="<?php echo $model->ring->cost_per_dwt;?>" />
  <input type="hidden" name="alias" value="<?php echo $model->ring->alias;?>" />
  <input type="hidden" name="setting_style" value="<?php echo $model->ring->setting_style;?>" />
  <input type="hidden" name="colors" value="<?=$model->ring->colors;?>" />
  <div role="tabpanel">
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
      <li role="presentation"><a href="#seo-tab" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li> 
      <li role="presentation"><a href="#categories-tab" aria-controls="categories" role="tab" data-toggle="tab">Categories</a></li> 
    
      <li role="presentation"><a href="#images-tab" aria-controls="images" role="tab" data-toggle="tab">Images</a></li>
     
      
    </ul>
    
    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="general-tab">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <h4>General</h4>
                    <div class="form-group">
                        <label>Name</label>
                        <?php echo $model->form->editorFor("name"); ?>
                    </div>

                    <div class="form-group">
                        <label>Featured</label>
                        <?php echo $model->form->checkBoxFor("featured", 1); ?>
                    </div>

                    <div class="form-group" style="<?=$model->ring->featured?'':'display:none'?>">
                        <label>Discount %</label>
                        <?php echo $model->form->editorFor("discount"); ?>
                    </div>
                    
                    <div class="form-group">
                      <label>Featured image</label>
                      <p><small>(ideal featured image size is 500 x 300)</small></p>
                      <p><input type="file" name="featured_image" class='image' /></p>
                      <div style="display:inline-block">
                      <?php
                        $img_path = "";
                        if($model->ring->featured_image != "" && file_exists(UPLOAD_PATH.'ring'.DS.$model->ring->featured_image)){
                            $img_path = UPLOAD_URL . 'rings/' . $model->ring->featured_image;
                      ?>
	                      <div class="well well-sm pull-left">
	                          <img src="<?php echo $img_path; ?>" width="100" />
	                          <br />
	                          <a href="<?= ADMIN_URL.'rings/delete_image/'.$model->ring->id;?>?featured_image=1" onclick="return confirm('Are you sure?');" class="btn btn-default btn-xs">Delete</a>
	                          <input type="hidden" name="featured_image" value="<?=$model->ring->featured_image?>" />
	                      </div>
                      <?php } ?>
                      <div class='preview-container'></div>
                      </div>
                  </div>

                    <div class="form-group">
                        <label>Featured Video</label>
                        <p>
                            <small>(ideal featured video is less then 10M)</small>
                        </p>
                        <p><input type="file" name="video_link" class='video'/></p>
                        <?php if($model->ring->video_link) { ?>
                            <div style="display:inline-block">
                                <video controls width="400" height="300">
                                    <source src="<?=$model->ring->videoLink()?>" type="video/mp4">
                                </video>
                                <div class='preview-container'><?=$model->ring->video_link?></div>
                            </div>
                        <? } ?>
                    </div>

                    <div class="form-group">
                        <label>Colors</label>
                        <?php echo $model->form->editorFor("default_color"); ?>
                    </div>
                    <div class="form-group">
                        <label>Metal</label>
                        <?php echo $model->form->editorFor("default_metal"); ?>
                    </div>
                     <div class="form-group">
                        <label>Description</label>
                        <?php echo $model->form->textAreaFor("description",['class'=>'ckeditor']); ?>
                    </div>

<!--                      <div class="form-group">-->
<!--                        <label>Shape</label>-->
<!--                        --><?php //echo $model->form->editorFor("default_shape"); ?>
<!--                    </div>  -->
<!--                    <div class="form-group">-->
<!--                        <label>Clarity</label>-->
<!--                        --><?php //echo $model->form->editorFor("clarity"); ?>
<!--                    </div> -->
                    <div class="form-group">
                        <label>Semi-Mount Weight</label>
                        <?php echo $model->form->editorFor("semi_mount_weight"); ?>
                    </div>
                     <div class="form-group">
                        <label>DWT</label>
                        <?php echo $model->form->editorFor("dwt"); ?>
                    </div>  
                     <div class="form-group">
                        <label>Stone Breakdown</label>
                        <?php echo $model->form->editorFor("stone_breakdown"); ?>
                    </div> 
                     <div class="form-group">
                        <label>Center Stone Weight</label>
                        <?php echo $model->form->editorFor("center_stone_weight"); ?>
                    </div> 
                     <div class="form-group">
                        <label>Center Stone Shape</label>
                        <?php echo $model->form->editorFor("center_stone_shape"); ?>
                    </div> 
                     <div class="form-group">
                        <label>total_stones</label>
                        <?php echo $model->form->editorFor("total_stones"); ?>
                    </div> 
                     <div class="form-group">
                        <label>Semi Mount Diamond Weight</label>
                        <?php echo $model->form->editorFor("semi_mount_diamond_weight"); ?>
                    </div> 
                     <div class="form-group">
                        <label>Diamond Color</label>
                        <?php echo $model->form->editorFor("diamond_color"); ?>
                    </div>
                     <div class="form-group">
                        <label>Diamond Quality</label>
                        <?php echo $model->form->editorFor("diamond_quality"); ?>
                    </div>
                     <div class="form-group">
                        <label>Setting Style</label>
                        <?php echo $model->form->editorFor("setting_style"); ?>
                    </div>

                    <!-- <div class="form-group">
                      <label>Brand</label>
                      <select name="brand_id">
                      	<option value="0">Please select a brand</option>
                        <?php foreach($model->brands as $b) {
                        	$selected = ($b->id == $model->product->brand_id) ? " selected='selected'" : "";
                        ?>
                        	<option value="<?php echo $b->id;?>"<?php echo $selected;?>><?php echo $b->name;?></option>
                      	<?php } ?>
                      </select>
                    </div> -->
                    <!-- <div class="form-group">
                      <label>Color</label>
                      <select name="color">
                      	<option value="0">Please select a color</option>
                      <?php foreach($model->colors as $c) {
                        $selected  = ($c->id==$model->product->color) ? " selected='selected'" : "";
                        ?>
                        <option value="<?php echo $c->id;?>"<?php echo $selected;?>><?php echo $c->name;?></option>
                      <?php } ?>
                      </select>
                    </div> -->

                    <div class="form-group">
                        <label>Featured?</label>
                        <?php echo $model->form->checkBoxFor("featured",1); ?>
                    </div>
                    <div class="form-group">
                        <label>Metals -> Colors</label>
                        <?$metal_colors = $model->ring->metal_colors?json_decode($model->ring->metal_colors):json_decode(\Model\Ring::defaultMetalColors());;
                        foreach($metal_colors as $metal=>$colors){?>
                        <div class="checkbox">
                            <label>
                                <input class="metal" type="checkbox" value="<?=$metal?>" name="metals[]" <?=strpos($model->ring->metals, $metal) === false?'':'checked'?>><?=$metal?>
                                <?foreach($colors as $color=>$val){?>
                                    <div class="checkbox">
                                        <label>
                                            <input class="color" type="checkbox" value="<?=$color?>" name="<?=$metal?>_colors[]" <?=$val == 0 ?'':'checked'?>><?=$color?>
                                        </label>
                                    </div>
                                <?}?>
                            </label>
                        </div>
                        <?}?>
                    </div>
                </div>
            </div>
<!--            Unused-->
<!--          <div class="col-md-12">-->
<!--                <div class="box">-->
<!--                    <h4>Prices</h4>-->
<!--                    <div class="form-group">-->
<!--                        <label>14kt Gold Price</label>-->
<!--                        --><?php //echo $model->form->editorFor("website_fourteenkt_gold_price"); ?>
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label>18kt Gold Price</label>-->
<!--                        --><?php //echo $model->form->editorFor("website_eighteenkt_gold_price"); ?>
<!--                    </div>-->
<!--                    <div class="form-group">-->
<!--                        <label>Platinum Price</label>-->
<!--                        --><?php //echo $model->form->editorFor("website_platinum_price"); ?>
<!--                    </div>-->
<!--                    <div class="form-group" style="display:none;">-->
<!--                        <label>MSRP Enabled?</label>-->
<!--                        --><?php //echo $model->form->checkBoxFor("msrp_enabled",1); ?>
<!--                    </div>-->
<!--                </div>-->
<!--          </div>-->
          <div class="col-md-12">
                <div class="box">
                    <h4>Diamond Fitting</h4>
                    <div class="form-group">
                        <label>Minimum Carat</label>
                        <?php echo $model->form->editorFor("min_carat"); ?>
                    </div>
                    <div class="form-group">
                        <label>Maximum Carat</label>
                        <?php echo $model->form->editorFor("max_carat"); ?>
                    </div>
<!--                    <div class="form-group">-->
<!--                        <label>Shape</label>-->
<!--                        --><?php //echo $model->form->editorFor("shape"); ?>
<!--                    </div>-->
                    <div class="form-group">
                        <label>Shapes</label>
                        <?foreach(\Model\Product::getDiamondShapes() as $short=>$shape){?>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="shape[]" value="<?=strtoupper($short)?>" <?=strpos($model->ring->shape, strtoupper($short)) === false?'':'checked';?>> <?=ucfirst($shape)?>
                                </label>
                            </div>
                        <?}?>
                    </div>
                </div>
          </div>
          <div class="col-md-12">
                <div class="box">
                    <h4>Expenses</h4>
                    <div class="form-group">
                        <label>14kt Gold Price</label>
                        <?php echo $model->form->editorFor("fourteenkt_gold_price"); ?>
                    </div>
                    <div class="form-group">
                        <label>18kt Gold Price</label>
                        <?php echo $model->form->editorFor("eighteenkt_gold_price"); ?>
                    </div>
                    <div class="form-group">
                        <label>Platinum Price</label>
                        <?php echo $model->form->editorFor("platinum_price"); ?>
                    </div>
                    <div class="form-group">
                        <label>Setting Labor</label>
                        <?php echo $model->form->editorFor("setting_labor"); ?>
                    </div>
                    <div class="form-group">
                        <label>Center Stone Setting Labor</label>
                        <?php echo $model->form->editorFor("center_stone_setting_labor"); ?>
                    </div>
                    <div class="form-group">
                        <label>Polish Labor</label>
                        <?php echo $model->form->editorFor("polish_labor"); ?>
                    </div>
                    <div class="form-group">
                        <label>Head to Add</label>
                        <?php echo $model->form->editorFor("head_to_add"); ?>
                    </div>
                    <div class="form-group">
                        <label>Melee Cost</label>
                        <?php echo $model->form->editorFor("melee_cost"); ?>
                    </div>
                    <div class="form-group">
                        <label>Shipping from Casting</label>
                        <?php echo $model->form->editorFor("shipping_from_cast"); ?>
                    </div>
                    <div class="form-group">
                        <label>Shipping to Customer</label>
                        <?php echo $model->form->editorFor("shipping_to_customer"); ?>
                    </div>
                    <div class="form-group">
                        <label>Total 14kt Cost</label>
<!--                        --><?php //echo $model->form->editorFor("total_14kt_cost"); ?>
                        <input readonly type="text" name="total_14kt_cost" value="<?=round($model->ring->total_14kt_cost,2)?>"/>
                    </div>
                    <div class="form-group">
                        <label>Total 18kt Cost</label>
<!--                        --><?php //echo $model->form->editorFor("total_18kt_cost"); ?>
                        <input readonly type="text" name="total_18kt_cost" value="<?=round($model->ring->total_18kt_cost,2)?>"/>
                    </div>
                    <div class="form-group">
                        <label>Total Platinum Cost</label>
<!--                        --><?php //echo $model->form->editorFor("total_plat_cost"); ?>
                        <input readonly type="text" name="total_plat_cost" value="<?=round($model->ring->total_plat_cost,2)?>"/>
                    </div>
                </div>
          </div>
          <!--<div class="col-md-12">
                <div class="box">
                    <h4>Size</h4>
                    <div class="form-group">
                        <label>Length</label>
                        <?php /*echo $model->form->textAreaFor('length'); */?>
                    </div>
                     <div class="form-group">
                        <label>Width</label>
                        <?php /*echo $model->form->textAreaFor('width'); */?>
                    </div>
                     <div class="form-group">
                        <label>depth</label>
                        <?php /*echo $model->form->textAreaFor('depth'); */?>
                    </div>
                     <div class="form-group">
                        <label>table_percent</label>
                        <?php /*echo $model->form->textAreaFor('table_percent'); */?>
                    </div>
                     <div class="form-group">
                        <label>girdle</label>
                        <?php /*echo $model->form->textAreaFor('girdle'); */?>
                    </div>
                     <div class="form-group">
                        <label>culet</label>
                        <?php /*echo $model->form->textAreaFor('culet'); */?>
                    </div>
                     <div class="form-group">
                        <label>certificate_file</label>
                        <?php /*echo $model->form->textAreaFor('certificate_file'); */?>
                    </div>
                     <div class="form-group">
                        <label>origin</label>
                        <?php /*echo $model->form->textAreaFor('origin'); */?>
                    </div>
                     <div class="form-group">
                        <label>memo_status</label>
                        <?php /*echo $model->form->textAreaFor('memo_status'); */?>
                    </div>
                     <div class="form-group">
                        <label>inscription</label>
                        <?php /*echo $model->form->textAreaFor('inscription'); */?>
                    </div>

                </div>
          </div>-->
          
        </div>
      </div>
      <div role="tabpanel" class="tab-pane" id="seo-tab">
         <div class="row">
            <div class="col-md-24">
                <div class="box">
                    <h4>SEO</h4>
                    <div class="form-group">
                        <label>Slug</label>
                        <?php echo $model->form->editorFor("slug"); ?>
                    </div>
                    <div class="form-group">
                        <label>Tags</label>
                        <?php echo $model->form->editorFor("tags"); ?>
                    </div>
                    <div class="form-group">
                        <label>Meta Title</label>
                        <?php echo $model->form->editorFor("meta_title"); ?>
                    </div>
                     <div class="form-group">
                        <label>Meta Keywords</label>
                        <?php echo $model->form->editorFor("meta_keywords"); ?>
                    </div>
                     <div class="form-group">
                        <label>Meta Description</label>
                        <?php echo $model->form->editorFor("meta_description"); ?>
                    </div>
                </div>
            </div>
         </div>
      </div>
    
     
      <div role="tabpanel" class="tab-pane" id="categories-tab">
          <?$children=[]?>
          <?$noChildren=[]?>
          <?php foreach($model->categories as $cat) {if($cat->parent_category != 0){$children[$cat->id] = $cat->parent_category;}} ?>
        <select name="product_category[]" class="multiselect" data-placeholder="Categories" multiple="multiple">
        <?php  foreach($model->categories as $cat) {
            foreach(array_unique($children) as $key => $value){
                if($value == $cat->id){?>
                    <optgroup label="<?=$cat->name?>">
                        <? foreach ($children as $catId => $parentCat) {
                            if($parentCat == $cat->id){?>
                                <option value="<?=$catId?>"><?echo \Model\Category::getItem($catId)->name?></option>
                            <?}
                        }?>
                    </optgroup>
                <?}
            }
            if($cat->parent_category == 0){
                array_push($noChildren, $cat->id);
            }
        } ?>
            <optgroup label="No Parent">
                <? foreach(array_values($noChildren) as $value){?>
                <option value="<?php echo $value;?>"><?php echo \Model\Category::getItem($value)->name;?></option>
            <?php } ?>
            </optgroup>


        </select>
      </div>

       
       
      <div role="tabpanel" class="tab-pane" id="images-tab">
        <div class="row">
          <div class="col-md-24">
            <div class="box">
        <div class="dropzone" id="dropzoneForm" action="<?php echo ADMIN_URL.'rings/upload_images/'.$model->ring->id;?>">
          
        </div>
        <button id="upload-dropzone" class="btn btn-danger">Upload</button><br />
        </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-24">
            <div class="box">
              <table id="image-container" class="table table-sortable-container">
                <thead>
                  <tr>
                    <th>Image</th>
                    <th>File Name</th>
                    <th>Display Order</th>
                    <th>Delete</th>
                  </tr>
                </thead>
             <tbody>
                 <?$ring = new \Model\Product_Image()?>
                  <?php foreach($ring->getProductImage($model->ring->id, "Ring") as $rImg) {

                    if($rImg->exists_image()) {
                    ?>
                  <tr data-image_id="<?php echo $rImg->id;?>">
                    <td><img src="<?php echo $rImg->get_image_url(); ?>" width="100" height="100" /></td>
                    <td><?php echo $rImg->image;?></td>
                    <td class="display-order-td"><?php echo $rImg->display_order;?></td>
                    <td class="text-center">
                      <a class="btn-actions delete-product-image" href="<?php echo ADMIN_URL; ?>rings/delete_prod_image/<?php echo $rImg->id; ?>?token_id=<?php echo get_token();?>">
                        <i class="icon-cancel-circled"></i> 
                      </a>
                    </td>
                  </tr>
                  <?php 
                    }
                    } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      
          
    </div>
  </div>
  <button type="submit" class="btn btn-save">Save</button>
</form>

<?php echo footer(); ?>
<script type="text/javascript">

	var colors = <?php echo json_encode(explode(',', $model->ring->default_color)); ?>;
//  	var sizes = <?php //echo json_encode(explode(',', $model->product->sizes)); ?>//;
  var categories = <?php echo json_encode($model->product_categories); ?>;
    // var collections = <?php //echo json_encode($model->product_collections); ?>;
    // var materials = <?php //echo json_encode($model->product_materials); ?>;
  //console.log(categories);
  var site_url=<?php echo json_encode(ADMIN_URL.'rings/'); ?>;
  $(document).ready(function() {
    //var feature_image = new mult_image($("input[name='featured_image']"),$("#preview-container"));
    var video_thumbnail = new mult_image($("input[name='video_thumbnail']"),$("#preview-thumbnail-container"));
    	
    	function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				var img = $("<img />");
				reader.onload = function (e) {
					img.attr('src',e.target.result);
					img.attr('alt','Uploaded Image');
					img.attr("width",'100');
					img.attr('height','100');
				};
				$(input).parent().parent().find('.preview-container').html(img);
				$(input).parent().parent().find('input[type="hidden"]').remove();

				reader.readAsDataURL(input.files[0]);
			}
		}

		$("input.image").change(function(){
			readURL(this);
		});


//		$("input[name='name']").on('keyup',function(e) {
//			var val = $(this).val();
//			val = val.replace(/[^\w-]/g, '-');
//			val = val.replace(/[-]+/g,'-');
//			$("input[name='slug']").val(val.toLowerCase());
//		});
        
  $("select.multiselect").each(function(i,e) {
            $(e).val('');
            var placeholder = $(e).data('placeholder');
            $(e).multiselect({
                nonSelectedText:placeholder,
                includeSelectAllOption: true,
                maxHeight: 415,
                checkboxName: '',
                enableCaseInsensitiveFiltering: true,
                buttonWidth: '100%'
            });
        });
  		  $("select[name='colors[]']").val(colors);
//  		  $("select[name='sizes[]']").val(sizes);
          $("select[name='product_category[]']").val(categories);
          //$("select[name='product_collection[]']").val(collections);
          //$("select[name='product_material[]']").val(materials);
          $("select.multiselect").multiselect("rebuild");
          
          function sort_number_display() {
        var counter = 1;
         $('#image-container >tbody>tr').each(function(i,e) {
           $(e).find('td.display-order-td').html(counter);
           counter++;
         });
      }
      $("a.delete-product-image").on("click",function() {
        if(confirm("are you sure?")) {
          location.href=$(this).attr("href");
        } else {
          return false;
        }
      });
      $('#image-container').sortable({
          containerSelector: 'table',
          itemPath: '> tbody',
          itemSelector: 'tr',
          placeholder: '<tr class="placeholder"/>',
          onDrop: function ($item, container, _super, event) {
            
              if($(event.target).hasClass('delete-product-image')) {
                  $(event.target).trigger('click');
              } 

              var ids = [];
              var tr_containers = $("#image-container > tbody > tr");
              tr_containers.each(function(i,e) {
                  ids.push($(e).data("image_id"));

              });
              $.post(site_url+'sort_images', {ids: ids}, function(response){
                sort_number_display();
              });
              _super($item);
          }
        });
      $('.metal').on('change', function(){
          if($(this).is(':checked')) {
              $(this).siblings().find('.color').prop('checked', true);
          } else {
              $(this).siblings().find('.color').prop('checked', false);
          }
      });
      $('.color').on('change', function(){
          if($(this).is(':checked')){
              $(this).parent().parent().siblings('.metal').prop('checked',true);
          }
      });

      $("input[name='featured']").on('click', function(){
          if($(this).is(':checked')){
              $("input[name='discount']").parent().show();
          } else {
              $("input[name='discount']").parent().hide();
              $("input[name='discount']").val(0);
          }
      });
      });
</script>
<script type="text/javascript">
Dropzone.options.dropzoneForm = { // The camelized version of the ID of the form element

  // The configuration we've talked about above
  autoProcessQueue: false,
  uploadMultiple: true,
  parallelUploads: 100,
  maxFiles: 100,
   url: <?php  echo json_encode(ADMIN_URL.'rings/upload_images/'.$model->ring->id);?>,
  // The setting up of the dropzone
  init: function() {
    var myDropzone = this;

    // First change the button to actually tell Dropzone to process the queue.
    $("#upload-dropzone").on("click", function(e) {
      // Make sure that the form isn't actually being sent.
      e.preventDefault();
      e.stopPropagation();
      myDropzone.processQueue();
    });

    // Listen to the sendingmultiple event. In this case, it's the sendingmultiple event instead
    // of the sending event because uploadMultiple is set to true.
    this.on("sendingmultiple", function() {
      // Gets triggered when the form is actually being sent.
      // Hide the success button or the complete form.
      $("#upload-dropzone").prop("disabled",true);
    });
    this.on("successmultiple", function(files, response) {
      // Gets triggered when the files have successfully been sent.
      // Redirect user or notify of success.
      window.location.reload();
      $("#upload-dropzone").prop("disabled",false);
    });
    this.on("errormultiple", function(files, response) {
      // Gets triggered when there was an error sending the files.
      // Maybe show form again, and notify user of error
    });
  }

}
</script>