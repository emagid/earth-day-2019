<main class='content'>
	<div class='home'>
		<section class='pic_block right no_txt' id='action'>
			<div>
				<h2>TAKE ACTION NOW</h2>
				<button id=''>DO JUST 1 THING</button>
			</div>
			<img src="<?= FRONT_ASSETS ?>img/img1.jpg">
		</section>
		<section id='photobooth' class='img_txt' style='background-image: url("<?= FRONT_ASSETS ?>img/img2.jpg");'>
			<img src="<?= FRONT_ASSETS ?>img/red_camera.png">
			<h2>TAKE A PHOTO</h2>
		</section>
		<section id='energy' class='pic_block left no_txt'>
			<div>
				<h2>SIGN UP FOR CLEAN ENERGY NOW</h2>
				<button id=''>JOIN NOW</button>
			</div>
			<img src="<?= FRONT_ASSETS ?>img/img3.jpg">
		</section>
	</div>
</main>