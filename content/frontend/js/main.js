$(document).ready(function(){


  // opening animations
  $('.white').fadeOut(500);


    // no rights clicks
    document.addEventListener('contextmenu', event => event.preventDefault());


    // Timeout
    var initial = null;

        function invoke() {
            var kiosk = $('.kiosk_check').attr('id');
            if ( kiosk ) {
                initial = window.setTimeout(
                    function() {
                        $('.white').fadeIn();
                        setTimeout(function(){
                            window.location.href = '/home/' + kiosk;
                    }, 500);
                }, 90000);
            }
        }


        invoke();
        $('body').on('click mousemove', function(){
            window.clearTimeout(initial);
            invoke();
        })




    // =====================  Link navigation ===========================
    $(document).on('click', '.pic_block', function(){
        fade(this);
        $(this).children('img').addClass('enlarge');
        nextPage(this);
    });

    $(document).on('click', '.img_txt', function(){
        fade(this);
        $(this).addClass('enlarge');
        nextPage(this);
    });

    function fade(el) {
        $('main section').addClass('fade');
        $(el).removeClass('fade');
    }

    function nextPage(el) {
        $('main section').css('pointer-events', 'none');
        var pg = $(el).attr('id');
        if ( pg == "photobooth" ) {
            $('.white').fadeIn(1000);
            timer = setTimeout(function(){
                window.location = '/home/photobooth';
            }, 2000);
        }else {
            $.ajax({
                url:'/home/'+pg,
                    type:'GET',
                    success: function(data){
                    var timer;
                        timer = setTimeout(function(){
                            $('.white').fadeIn(1000);
                            timer = setTimeout(function(){
                                $('.white').fadeOut(500);
                                $('.content').html($(data).find('.content').html()).fadeIn();
                            }, 2000);
                        }, 100);
                    }
            });
        }


    }   


    navTime = 0;
    $(document).on('click', '.navs nav', function(){
        $('.navs nav').css('pointer-events', 'none');
        $('.load').fadeIn(500);
        $('.load').css('display', 'flex');

        clearTimeout(navTime);
        $('.navs nav').removeClass('active');
        $(this).addClass('active');
        var id = $(this).attr('id');
        $.ajax({
            url:id,
                type:'GET',
                success: function(data){
                var timer;
                  $('.jQKeyboardContainer').hide();
                    timer = setTimeout(function(){
                        $('.content').fadeOut(500);
                        timer = setTimeout(function(){
                            $('.navs nav').css('pointer-events', 'all');
                            $('.load').fadeOut(500);
                            $('.content').html($(data).filter('.content').html()).fadeIn();
                            // init();
                        }, 2000);
                    }, 100);
                }
        });
    });
        


});



