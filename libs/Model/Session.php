<?php

namespace Model;

class Session extends \Emagid\Core\Model {
    static $tablename = "public.session";

    public static $fields  =  [
        'title',
        'date_time',
    ];

}