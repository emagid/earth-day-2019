<?php
namespace Model;

class Calendar extends \Emagid\Core\Model {
	static $tablename = "calendar";
	public static $fields = [
  		'day',
  		'time_start',
  		'time_over',
  		'provider_id'
  	];
}