<?php
/**
 * Created by PhpStorm.
 * User: Janus
 * Date: 11/17/15
 * Time: 1:52 PM
 */

namespace Model;

class Ring_Material extends \Emagid\Core\Model {
    public static $tablename = "ring_material";

    public static $fields = [
        "name","price", "min_size", "max_size", "size_step", "size_step_price"
    ];

    public function size_price($selectedSize)
    {
        return (($selectedSize - $this->min_size)/$this->size_step) * $this->size_step_price;
    }
}